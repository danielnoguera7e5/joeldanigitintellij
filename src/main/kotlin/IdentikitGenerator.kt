
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val cabells = scanner.nextLine()
    val ulls = scanner.nextLine()
    val nas = scanner.nextLine()
    val boca = scanner.nextLine()

    when (cabells) {
        "arrissats" -> println("@@@@@")
        "llisos" -> println("VVVVV")
        "pentinats" -> println("XXXXX")
        "gat" -> println("∧___∧")
    }

    when (ulls) {
        "aclucats" -> println(".-.-.")
        "rodons" -> println(".o-o.")
        "estrellats" -> println(".*-*.")
        "flors" -> println(".✿-✿.")
    }

    when (nas) {
        "aixafat" -> println("..0..")
        "arromangat" -> println("..C..")
        "aguilenc" -> println("..V..")
        "rodo" -> println("..ʖ..")
    }

    when (boca) {
        "normal" -> println(".===.")
        "bigoti" -> println(".∼∼∼.")
        "dents-sortides" -> println(".www.")
        "oberta" -> println(("..O..")
    }
}